using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items
{
    public enum ItemType
    {
        Food
    }

    [CreateAssetMenu(fileName = "New Item", menuName = "Items/Item")]
    public class ItemData : ScriptableObject
    {
        public ItemType type;
        public Item prefab;
    }
}