using Interactivity;
using Items.Containers;
using ObjectPooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items
{
    /// <summary>
    /// Can be food, props, etc...
    /// </summary>
    public class Item : PoolParty<Item>, IInteractive
    {
        public ItemData data;
        public bool isInteractive;

        [Header("References")]
        public Transform T;
        public Rigidbody rb;

        public void InteractWithPlayer()
        {
            Player.Player.I.PickupItem(this);
        }

        public bool IsInteractive()
        {
            return isInteractive;
        }

        public override void Pool_Spawn(Vector3 position, Quaternion rotation)
        {
            T.position = position;
            T.rotation = rotation;
            gameObject.SetActive(true);
        }

        public override void Pool_Despawn()
        {
            gameObject.SetActive(false);
        }

        /*
        private void OnCollisionEnter(Collision collision)
        {
            if (rb.velocity.magnitude < 3)
                return;

            if (collision.gameObject.CompareTag("Table"))
            {
                collision.gameObject.GetComponent<KitchenCounter>().Store(this);
            }
        }*/
    }
}