using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities {
    public class LookAt : MonoBehaviour
    {
        [SerializeField] private Transform T;
        [SerializeField] private Transform targetT;
        [SerializeField] private bool invert;

        private void Update()
        {
            if(invert)
                T.rotation = Quaternion.LookRotation(targetT.position - T.position);
            else
                T.rotation = Quaternion.LookRotation(T.position - targetT.position);
        }
    }
}