using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stuck : MonoBehaviour
{
    [SerializeField] private Transform T;
    [SerializeField] private Transform targetT;

    private void Update()
    {
        T.position = targetT.position;
    }
}
