using Items;
using Items.Containers;
using Items.Food;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Timers;

public class Stove : FoodPad
{
    [SerializeField] private ParticleSystem fireVFX;
    private bool isCooking = false;
    private Timer timer = new Timer();

    private void Update()
    {
        if (!isCooking)
            return;

        if (timer.HasFinishedCountdown())
        {
            Debug.Log("Has finished");
            StopCooking();

            Item cookedItem = Instantiate(((CookableFoodData)storedItem.data).cookedData.prefab);
            storedItem.myPool.DespawnEntity(storedItem);
            Clear();
            Store(cookedItem);
        }
    }

    public override void InteractWithPlayer()
    {
        //can't withdraw food while its cooking
        if (isCooking)
            return;

        //player wants to pickup cooked food
        if(storedItem != null) {
            Item item = storedItem;
            Puke();
            Player.Player.I.PickupItem(item);
            return;
        }

        //try cooking the food in the player's holding
        if (Player.Player.I.itemInHandData.type == ItemType.Food)
        {
            if (((FoodData)Player.Player.I.itemInHandData).state != FoodState.IsRaw)
                return;
        }
        else
            return;

        Store(Player.Player.I.StealItem());
    }

    public override bool IsInteractive()
    {
        return !isCooking && (storedItem != null || (Player.Player.I.isHoldingItem && Player.Player.I.itemInHandData.type == ItemType.Food && ((FoodData)Player.Player.I.itemInHandData).state == FoodState.IsRaw));
    }

    public override void Store(Item item)
    {
        base.Store(item);

        if (((FoodData)item.data).state != FoodState.IsRaw)
            return;

        fireVFX.Play();

        isCooking = true;
        timer.StartCountdown(5);
    }

    public override void Puke()
    {
        if (isCooking)
        {
            StopCooking();
        }

        base.Puke();
    }

    private void StopCooking()
    {
        isCooking = false;
        fireVFX.Stop();
    }
}
