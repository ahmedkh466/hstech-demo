using Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items.Food
{
    [CreateAssetMenu(fileName = "New Food", menuName = "Items/Cookable Food")]
    public class CookableFoodData : FoodData
    {
        public FoodData cookedData;
    }
}