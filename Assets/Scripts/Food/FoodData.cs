using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items.Food
{
    public enum FoodState
    {
        /// <summary>
        /// Can be cooked.
        /// </summary>
        IsRaw,
        IsCooked,
        CannotBeCooked
    }
    [CreateAssetMenu(fileName = "New Food", menuName = "Items/Food")]
    public class FoodData : ItemData
    {
        public FoodState state;
        public int sellPrice;
    }
}