using Items;
using Items.Food;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CashRegister : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI revenueText;
    private int revenue = 0;

    private void OnTriggerEnter(Collider collision)
    {
        Item item = collision.gameObject.GetComponent<Item>();
        if (item == null)
            return;

        if (item.data.type != ItemType.Food)
            return;

        if (((FoodData)item.data).state != FoodState.IsCooked)
            return;

        revenue += ((FoodData)item.data).sellPrice;
        revenueText.text = revenue.ToString() +"$";
    }
}
