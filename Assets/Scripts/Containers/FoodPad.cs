using Interactivity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items.Containers
{
    /// <summary>
    /// Stores 1 of any item, behaves like a table.
    /// </summary>
    public class FoodPad : Container, IInteractive
    {
        protected const float PukeForce = 1000;

        [SerializeField] private InteractiveTarget interactivity;
        [SerializeField] private Vector3 itemPos;

        public virtual bool IsInteractive()
        {
            return storedItem != null || Player.Player.I.isHoldingItem;
        }

        public virtual void InteractWithPlayer()
        {
            if (Player.Player.I.isHoldingItem)
            {
                Store(Player.Player.I.StealItem());
            }else if(storedItem != null)
            {
                Item item = storedItem;
                Puke();
                Player.Player.I.PickupItem(item);
            }
        }

        public override void Store(Item item)
        {
            base.Store(item);
            storedItem.rb.isKinematic = true;
            storedItem.T.position = transform.position + itemPos;
            storedItem.T.rotation = Quaternion.identity;
            storedItem.isInteractive = false;
        }

        public override void Puke()
        {
            storedItem.rb.isKinematic = false;
            //storedItem.rb.AddForce(Vector3.up * PukeForce + transform.forward * PukeForce*0.6f);
            storedItem.rb.position = transform.position + transform.forward * 2;
            storedItem.isInteractive = true;
            base.Puke();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position + itemPos, 0.03f);
        }
    }
}