using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items.Containers
{
    /// <summary>
    /// Can store 1 item.
    /// </summary>
    public class Container : MonoBehaviour
    {
        public Item storedItem { get; private set; }

        public virtual void Store(Item item)
        {
            if (storedItem != null)
            {
                Puke();
            }

            storedItem = item;
        }

        /// <summary>
        /// Properly removes the item from storage.
        /// </summary>
        public virtual void Puke()
        {
            storedItem = null;
        }

        /// <summary>
        /// Removes the item as if it was never stored.
        /// </summary>
        public virtual void Clear()
        {
            storedItem = null;
        }
    }
}