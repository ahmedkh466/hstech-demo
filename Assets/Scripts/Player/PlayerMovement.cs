using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.Movement
{
    public class PlayerMovement : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform T;
        [SerializeField] private Rigidbody RB;
        [SerializeField] private Transform cameraT;

        [Header("Parameters")]
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationDamp;

        public Vector2 input;

        private Quaternion rotation;
        private Vector3 direction;

        private void Start()
        {
            rotation = RB.rotation;
        }

        private void FixedUpdate()
        {
            RB.rotation = Quaternion.Lerp(RB.rotation, rotation, Time.fixedDeltaTime * rotationDamp); 

            if (input == Vector2.zero)
                return;

            //direction = (input.x * T.right + input.y * T.forward).normalized;
            RB.AddForce(T.forward * movementSpeed * Time.fixedDeltaTime);
            rotation = Quaternion.LookRotation(FlatVector3(cameraT.forward) * input.y + FlatVector3(cameraT.right) * input.x, Vector3.up);
        }

        /// <summary>
        /// </summary>
        /// <returns>The same vector3 but with Y = 0 and magnitude 1.</returns>
        private Vector3 FlatVector3(Vector3 vec)
        {
            return new Vector3(vec.x, 0, vec.z).normalized;
        }
    }
}