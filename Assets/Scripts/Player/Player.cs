using Interactivity;
using Items;
using Items.Containers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Player : MonoBehaviour
    {
        public static Player I;

        [SerializeField] private Transform itemParentT;

        [Header("Interaction")]
        [SerializeField] private SphericalRadar radar;
        [SerializeField] private ObjectPointer targetPointer;

        [Header("Animation")]
        [SerializeField] private float itemPickupDamp;
        private Item itemInHands = null;
        public bool isHoldingItem
        {
            get
            {
                return itemInHands != null;
            }
        }
        public ItemData itemInHandData
        {
            get
            {
                if (itemInHands == null)
                    return null;
                return itemInHands.data;
            }
        }

        private void Awake()
        {
            I = this;
        }

        private void Update()
        {
            if (itemInHands == null)
                return;

            itemInHands.T.position = Vector3.Lerp(itemInHands.T.position, itemParentT.position, Time.deltaTime * itemPickupDamp);
        }

        private void OnEnable()
        {
            radar.OnTargetUpdated += OnPrioritizedTargetUpdated;
        }

        private void OnDisable()
        {
            radar.OnTargetUpdated -= OnPrioritizedTargetUpdated;
        }

        public void Input_Interact()
        {
            if (radar.prioritizedTarget == null)
            {
                if (itemInHands != null)
                    Input_Puke();
                return;
            }

            radar.prioritizedTarget.Interact();
        }

        public void Input_Puke()
        {
            if (itemInHands == null)
                return;

            Puke(700);
        }

        /// <summary>
        /// Throws away the item in hands.
        /// </summary>
        private void Puke(float force)
        {
            itemInHands.rb.isKinematic = false;
            //itemInHands.T.SetParent(null);
            itemInHands.rb.AddForce(itemParentT.forward * force + Vector3.down * force * 0.2f);

            itemInHands.isInteractive = true;

            itemInHands = null;
        }

        public void PickupItem(Item item)
        {
            if (itemInHands != null)
                Puke(170);

            item.rb.isKinematic = true;
            //item.T.SetParent(itemParentT);
            //item.T.position = itemParentT.position;

            item.isInteractive = false;

            itemInHands = item;
        }

        public Item StealItem()
        {
            Item item = itemInHands;
            itemInHands = null;

            return item;
        }

        public void OnPrioritizedTargetUpdated()
        {
            if (radar.prioritizedTarget == null)
            {
                targetPointer.PointAt(null);
                return;
            }

            targetPointer.PointAt(radar.prioritizedTarget);
            //target.Highlight(true);
            //targetInRadius = target;
        }
    }
}