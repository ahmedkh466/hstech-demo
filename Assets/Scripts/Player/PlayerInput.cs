using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.Movement
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private PlayerMovement movement;
        [SerializeField] private Player self;

        private void Update()
        {
            movement.input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            if (Input.GetButtonDown("Interact"))
            {
                self.Input_Interact();
            }else if (Input.GetButtonDown("Puke"))
            {
                self.Input_Puke();
            }
        }
    }
}