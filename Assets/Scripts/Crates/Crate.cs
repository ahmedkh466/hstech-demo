using Interactivity;
using Items;
using ObjectPooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items.Spawners
{
    /// <summary>
    /// When interacted with, will spawn an item of any type.
    /// </summary>
    public class Crate : MonoBehaviour, IInteractive
    {
        [SerializeField] private InteractiveTarget interactivity;
        [SerializeField] private Pool<Item> itemPool;

        public void InteractWithPlayer()
        {
            Item item = itemPool.SpawnUnit(transform.position, Quaternion.identity);
            Player.Player.I.PickupItem(item);
        }

        public bool IsInteractive()
        {
            return true;
        }
    }
}