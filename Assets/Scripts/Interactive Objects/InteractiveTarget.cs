using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Interactivity
{
    public interface IInteractive
    {
        public bool IsInteractive();
        public void InteractWithPlayer();
    }

    public class InteractiveTarget : MonoBehaviour
    {
        [Header("References")]
        public Transform T;
        private IInteractive target;

        [Header("Settings")]
        public int priority;
        public Vector3 pointerOffset;

        private void Awake()
        {
            target = GetComponent<IInteractive>();
        }

        public bool IsInteractive
        {
            get
            {
                return target.IsInteractive();
            }
        }

        public void Interact()
        {
            target.InteractWithPlayer();
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(T.position, T.position + pointerOffset);
        }
    }
}