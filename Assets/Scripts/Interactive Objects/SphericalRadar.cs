using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Interactivity
{
    /// <summary>
    /// Searches for interactive items within a radius.
    /// </summary>
    public class SphericalRadar : MonoBehaviour
    {
        [Header("Parameters")]
        [Tooltip("Example: Kitchen Counter, Item, etc...")]
        [SerializeField] private Transform centerT;
        [SerializeField] private float radius;

        private Collider[] collidersInRadius;
        public List<InteractiveTarget> targetsInRadius { get; private set; } = new List<InteractiveTarget>();
        public InteractiveTarget prioritizedTarget { get; private set; }

#if UNITY_EDITOR
        [SerializeField] private bool GIZMOS_draw;
        [SerializeField] private bool GIZMOS_fill;
#endif

        public UnityAction OnTargetUpdated;

        private void Update()
        {
            targetsInRadius.Clear();

            collidersInRadius = Physics.OverlapSphere(centerT.position, radius);
            if (collidersInRadius == null || collidersInRadius.Length == 0)
            {
                if(this.prioritizedTarget != null)
                {
                    this.prioritizedTarget = null;
                    OnTargetUpdated();
                }
                return;
            }

            InteractiveTarget target = null; //Temporary var
            InteractiveTarget newPrioritizedTarget = null;
            foreach (Collider collider in collidersInRadius)
            {
                target = collider.GetComponent<InteractiveTarget>();
                if (target == null || !target.IsInteractive)
                    continue;

                targetsInRadius.Add(target);

                if (newPrioritizedTarget == null)
                {
                    newPrioritizedTarget = target;
                }else if (newPrioritizedTarget.priority < target.priority) //Prioritize higher priority targets
                {
                    newPrioritizedTarget = target;
                }else if (newPrioritizedTarget.priority == target.priority && Vector3.Distance(target.T.position, centerT.position) < Vector3.Distance(newPrioritizedTarget.T.position, centerT.position)) //Compare distance between targets of the same priority level
                {
                    newPrioritizedTarget = target;
                }
            }

            if(this.prioritizedTarget != newPrioritizedTarget)
            {
                this.prioritizedTarget = newPrioritizedTarget;
                OnTargetUpdated();
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (!GIZMOS_draw)
                return;

            if (GIZMOS_fill)
                Gizmos.DrawSphere(centerT.position, radius);
            else
                Gizmos.DrawWireSphere(centerT.position, radius);
        }
#endif
    }
}