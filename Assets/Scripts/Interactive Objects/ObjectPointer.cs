using Interactivity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interactivity
{
    public class ObjectPointer : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform T;
        [SerializeField] private GameObject icon;
        private InteractiveTarget target;

        private void Update()
        {
            if (target == null)
            {
                gameObject.SetActive(false);
                return;
            }

            T.position = target.T.position + target.pointerOffset.x * target.T.right + target.pointerOffset.y * Vector3.up + target.pointerOffset.z * target.T.forward;
        }

        public void PointAt(InteractiveTarget target)
        {
            if (target == null)
            {
                gameObject.SetActive(false);
                return;
            }

            T.position = target.T.position + target.pointerOffset.x * target.T.right + target.pointerOffset.y * Vector3.up + target.pointerOffset.z * target.T.forward;
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            this.target = target;
        }

        private void OnEnable()
        {
            icon.SetActive(true);
        }

        private void OnDisable()
        {
            if (icon != null)
                icon.SetActive(false);
        }
    }
}