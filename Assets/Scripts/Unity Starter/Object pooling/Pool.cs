using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling
{
    public enum PoolPartyState
    {
        Spawned,
        Despawned,
        Despawning //Delayed despawn
    }

    public abstract class PoolParty<EntityType> : MonoBehaviour where EntityType : PoolParty<EntityType>
    {
        public Pool<EntityType> myPool { get; private set; }
        public PoolPartyState pool_State { get; private set; }

        public void Pool_Setup(Pool<EntityType> pool)
        {
            myPool = pool;
        }

        public void Pool_SetState(PoolPartyState newState)
        {
            pool_State = newState;
        }

        public void OnDestroy()
        {
            if(myPool != null)
                myPool.OnEntityDestroyed((EntityType)this);
        }

        public abstract void Pool_Spawn(Vector3 position, Quaternion rotation);
        public abstract void Pool_Despawn();
    }

    [System.Serializable]
    public class Pool<EntityType> where EntityType : PoolParty<EntityType>
    {
        [SerializeField] private EntityType prefab;
        [SerializeField] private Transform disabledEntitiesParent;
        [SerializeField] private int maxInstances;
        [SerializeField] private int initialInstances;
        private List<EntityType> entities = new List<EntityType>();

        public void Initialize()
        {
            EntityType entity = null;
            for (int i = 0; i < initialInstances; i++)
            {
                entity = InstantiateNewUnit();
                entity.Pool_Despawn();
                entity.Pool_SetState(PoolPartyState.Despawned);
            }
        }

        /// <summary>
        /// Retrieves a gameobject from the pool, if none is available, a new one will be instantiated.
        /// </summary>
        /// <returns></returns>
        public EntityType SpawnUnit(Vector3 position, Quaternion rotation)
        {
            EntityType entity = null;
            for(int i = 0;i<entities.Count;) 
            {
                entity = entities[i];
                if (entity == null)
                {
                    entities.RemoveAt(i);
                    continue;
                }

                if (entity.pool_State == PoolPartyState.Despawned)
                {
                    entity.Pool_Spawn(position, rotation);
                    entity.Pool_SetState(PoolPartyState.Spawned);
                    return entity;
                }

                i++;
            }

            entity = InstantiateNewUnit();
            entity.Pool_Spawn(position, rotation);
            entity.Pool_SetState(PoolPartyState.Spawned);
            return entity;
        }

        /// <summary>
        /// Spawns a unit and destroys it after specified lifetime
        /// </summary>
        /// <param name="position"></param>
        public EntityType SpawnTemporaryUnit(Vector3 position, Quaternion rotation, float lifetime)
        {
            EntityType entity = SpawnUnit(position, rotation);
            DespawnUnitDelayed(entity, lifetime);
            return entity;
        }

        /// <summary>
        /// Spawns a unit and destroys it after specified lifetime.<br/>
        /// Position: 0, 0, 0<br/>
        /// Rotation: 0, 0, 0
        /// </summary>
        /// <param name="position"></param>
        public EntityType SpawnTemporaryUnit(float lifetime)
        {
            EntityType entity = SpawnUnit(Vector3.zero, Quaternion.identity);
            DespawnUnitDelayed(entity, lifetime);
            return entity;
        }

        public void DespawnUnitDelayed(EntityType entity, float delay)
        {
            PoolsManager.I.StartCoroutine(DespawnUnitCoroutine(entity, delay));
        }

        private IEnumerator DespawnUnitCoroutine(EntityType entity, float delay)
        {
            entity.Pool_SetState(PoolPartyState.Despawning);
            yield return new WaitForSeconds(delay);
            DespawnEntity(entity);
        }

        /// <summary>
        /// If the number of available units is greater than the pool capacity, this unit will be destroyed
        /// </summary>
        /// <param name="entity"></param>
        public void DespawnEntity(EntityType entity)
        {
            if (entities.Count < maxInstances)
            {
                entity.Pool_Despawn();
                entity.Pool_SetState(PoolPartyState.Despawned);
            }
            else
            {
                Object.Destroy(entity.gameObject);
            }
        }

        public void OnEntityDestroyed(EntityType entity)
        {
            entities.Remove(entity);
        }

        private EntityType InstantiateNewUnit()
        {
            EntityType entity = Object.Instantiate(prefab, disabledEntitiesParent);
            entity.Pool_Setup(this);
            entities.Add(entity);
            return entity;
        }
    }
}