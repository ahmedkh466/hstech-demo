using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling
{
    public class PoolsManager : MonoBehaviour
    {
        public static PoolsManager I;

        private void Awake()
        {
            I = this;
        }
    }
}