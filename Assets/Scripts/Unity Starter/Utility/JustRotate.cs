using UnityEngine;

public class JustRotate : MonoBehaviour
{
    [SerializeField] private Transform T;
    [SerializeField] private Vector3 speed;

    private void Update()
    {
        T.Rotate(speed * Time.deltaTime);
    }
}
