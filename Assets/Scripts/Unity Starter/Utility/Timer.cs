using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Utilities.Timers
{
    public class Timer
    {
        private double countdownEnd;

        public Timer()
        {
        }

        public void StartCountdown(float seconds)
        {
            countdownEnd = Time.timeAsDouble + seconds;
        }

        public double TimeRemaining
        {
            get
            {
                return countdownEnd - Time.timeAsDouble;
            }
        }

        public bool HasFinishedCountdown()
        {
            return Time.timeAsDouble > countdownEnd;
        }

        public static IEnumerator TimerCoroutine(float time, UnityAction callback)
        {
            yield return new WaitForSeconds(time);
            callback();
        }
    }
}